title:      "Reading 02: Hiring, Negotiations, Contracts, Salaries, Promotion, Mobility"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for **Tuesday, January 31** are:

  ### Hiring

  1. [Why Can't Programmers.. Program?](http://blog.codinghorror.com/why-cant-programmers-program/)

  2. [Why is hiring broken? It starts at the whiteboard.](https://medium.freecodecamp.com/why-is-hiring-broken-it-starts-at-the-whiteboard-34b088e5a5db#.qp7u5ik1t)

  3. [Here's Google's Secret to Hiring the Best People](http://www.wired.com/2015/04/hire-like-google/)

  4. [On the unreasonable reality of "junior" developer interviews](https://medium.com/@samphippen/on-the-unreasonable-reality-of-junior-developer-interviews-946498c0ad57#.6hfzby5dv)

  5. [We Hire the Best, Just Like Everyone Else](https://blog.codinghorror.com/we-hire-the-best-just-like-everyone-else/)

  6. [We only hire the best means we only hire the trendiest](http://danluu.com/programmer-moneyball/)

  ### Negotiations

  1. [Salary Negotiation: Make More Money, Be More Valued](http://www.kalzumeus.com/2012/01/23/salary-negotiation/)

  2. [High Salaries Haunt Some Job Hunters](https://www.ilr.cornell.edu/sites/ilr.cornell.edu/files/High%20Salaries%20Haunt%20Some%20Job%20Hunters%20-%20WSJ.pdf)

  3. [Why Recent Graduates Don't Negotiate](http://www.theatlantic.com/education/archive/2015/05/why-recent-graduates-dont-negotiate/392429/)

  4. [Why Women Don't Negotiate Their Job Offers](https://hbr.org/2014/06/why-women-dont-negotiate-their-job-offers/)

  5. [NDAs and Contracts That You Should Never Sign](http://www.joelonsoftware.com/articles/fog0000000071.html)

  6. [Getting a raise comes down to one thing: Leverage.](https://medium.freecodecamp.com/youre-underpaid-here-s-how-you-can-get-the-pay-raise-you-deserve-fafcf52956d6#.d79yn76kh)

  ### Promotion

  1. [What Is Stack Ranking? Microsoft Ends Controversial Employee Rating System, Yahoo Ramps It Up](http://www.ibtimes.com/what-stack-ranking-microsoft-ends-controversial-employee-rating-system-yahoo-ramps-it-1468850)

  2. [Why Stack Ranking Is A Terrible Way To Motivate Employees](http://www.businessinsider.com/stack-ranking-employees-is-a-bad-idea-2013-11)

  3. [Microsoft's Lost Decade](http://www.vanityfair.com/news/business/2012/08/microsoft-lost-mojo-steve-ballmer)

  4. [Amazon's Use of 'Stack' Ranking for Workers May Backfire, Experts Say](http://www.nbcnews.com/business/business-news/amazons-use-stack-ranking-workers-may-backfire-experts-say-n411306)

  5. [Management at Valve, as seen through the Valve Employee Handbook](https://blog.inf.ed.ac.uk/sapm/2014/03/14/management-at-valve-an-analysis-of-management-practices-at-valve-software/)

  6. [In big move, Accenture will get rid of annual performance reviews and rankings](https://www.washingtonpost.com/news/on-leadership/wp/2015/07/21/in-big-move-accenture-will-get-rid-of-annual-performance-reviews-and-rankings/)

  ### Mobility

  1. [You Should Plan On Switching Jobs Every Three Years For The Rest Of Your Life](http://www.fastcompany.com/3055035/the-future-of-work/you-should-plan-on-switching-jobs-every-three-years-for-the-rest-of-your-)

  2. [Litigation Over Noncompete Clauses Is Rising](http://www.wsj.com/articles/SB10001424127887323446404579011501388418552)

        You may need to search for this article to get a non-paywall version.

  3. [Noncompete Clauses Increasingly Pop Up in Array of Jobs](http://www.nytimes.com/2014/06/09/business/noncompete-clauses-increasingly-pop-up-in-array-of-jobs.html?_r=2&mtrref=undefined&gwh=FF5B42CDA1123085257024E64A173ECA&gwt=pay&assetType=nyt_now)

  4. [White House urges ban on non-compete agreements for many workers](http://www.reuters.com/article/us-usa-noncompetes-idUSKCN12P2YP)


  5. [How Companies Kill Their Employees' Job Searches](http://www.theatlantic.com/business/archive/2014/10/how-companies-kill-their-employees-job-searches/381437/)

  6. [Apple, Google will pay 64,000 engineers to avoid trial on "no-poach" deal](http://arstechnica.com/tech-policy/2014/04/apple-google-will-pay-64000-engineers-to-avoid-trial-on-no-poach-deal/)

  7. [Employees Who Stay In Companies Longer Than Two Years Get Paid 50% Less](http://www.forbes.com/sites/cameronkeng/2014/06/22/employees-that-stay-in-companies-longer-than-2-years-get-paid-50-less/#541b7d21210e)

  ## Questions

  Please write a response to one of the following questions:

  1. What has your job (or internship) interview process been like?  What
  surprises you?  What frustrates you?  What excites you?  How did you prepare?
  How did you perform?

      What is your overall impression of the general interview process?  Is it
      efficent? Is it effective? Is it humane? Is it ethical?

  2. Did you negotiate your contract (or do you plan to)? Why or why not?  On
  what points did you negotiate and how did that process go? Did you have to
  sign a [NDA] or [non-compete] in the process?  What sort of cool perks and
  bonuses did you get?

      What do you make of the negotiation process? Is it ethical to ask for
      more? Is it ethical to challenge or modify the terms of your contract?

  3. As you go through your job search process have you investigated how
  promotion and career advancement works at the companies you looked at?  What
  is the common career trajectory for employees at your company?  What systems
  of review and feedback do they have?

      What do you think of the stack ranking system common in tech companies?
      Is it fair?  Is it ethical?  How would you feel about participating in
      that process?

  4. Where do you see your career headed?  Do you plan on staying with one
  company or do you envision moving from job to job?

      Is there such thing as company loyalty?  Should you be loyal to your
      company and should your company be loyal to you?  How do things such as
      [non-competes] and [trade secrets] influence your opinion?  Are these
      contracts fair?  Are they ethical?  On the flip side, is job hopping an
      ethical practice?

  [NDA]:            https://en.wikipedia.org/wiki/Non-disclosure_agreement
  [non-compete]:    https://en.wikipedia.org/wiki/Non-compete_clause
  [non-competes]:   https://en.wikipedia.org/wiki/Non-compete_clause
  [trade secrets]:  https://en.wikipedia.org/wiki/Trade_secret

  <div class="alert alert-danger" markdown="1">
  #### <i class="fa fa-warning"></i> Pre-caution

  Because your blogs are public and tied to your name, I recommend that you
  don't use the names of real companies or people unless you are **100%**
  confident and secure in sharing that information.

  </div>

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.
