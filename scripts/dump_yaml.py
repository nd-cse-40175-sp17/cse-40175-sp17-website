#!/usr/bin/env python2.7

import sys

import yaml

# Functions

def dump_yaml(path):
    data   = yaml.load(open(path))
    fields = data[0].keys()

    for field in fields:
        print '{:10}'.format(field),
    print
    
    for record in data:
        for field in fields:
            print '{:10}'.format(record.get(field, '')),
        print

# Main Execution

if __name__ == '__main__':
    for path in sys.argv[1:]:
        dump_yaml(path)
