#!/usr/bin/env python2

import csv
import yaml

blogs = []

for blog in csv.DictReader(open('data/form.csv')):
    blogs.append({
        'netid':    blog['NetID'],
        'url':      blog['URL'],
    })

print yaml.dump(sorted(blogs, key=lambda b: b['netid']), default_flow_style=False)
